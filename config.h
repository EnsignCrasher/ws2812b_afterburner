#define NUM_STRIPS  3

int num_leds[NUM_STRIPS] = { 3, 3, 3 };
#define MAX_NUM_LEDS 3

#define LED_PIN     7
#define PWM_PIN     3
#define GEAR_PWM_PIN 4
