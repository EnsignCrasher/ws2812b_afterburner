#include <FastLED.h>
#include "config.h"

//#define NORX
//#define NOLED

#define PWM_P 1000 //PWM pulse length
#define PWM_MIN 1000
#define PWM_MAX 2000
CRGB leds[NUM_STRIPS][MAX_NUM_LEDS];

uint32_t PWM = 0;
uint32_t GEAR_PWM = 0;

#ifdef NORX || NOLED
#include "nohardware.h"
#endif

void led_show() {
#ifdef NOLED
  nohardware_led_show();
#else
  FastLED.show();
#endif
}

uint32_t readPWM(int pin) {
#ifdef NORX
  return pulseInSim();
#else
  return pulseIn(pin, HIGH, 50000);
#endif
}

void setLED(CRGB* target, CRGB* rgb, int strip_id){
  for (int i=0; i<num_leds[strip_id]; i++) {
    target[i] = *rgb;
  }
}

struct led_trigger {
  uint32_t timestamp;
  uint8_t state;
};
led_trigger trigger[NUM_STRIPS];

void random_color(CRGB *rgb, CRGB *MAX, CRGB *MIN) {
  rgb->red = (uint8_t)random(MIN->red, MAX->red);
  rgb->green = (uint8_t)random(MIN->green, MAX->green);
  rgb->blue = (uint8_t)random(MIN->blue, MAX->blue);
}

void setup() {
  pinMode(PWM_PIN, INPUT);

  FastLED.addLeds<WS2812, LED_PIN, GRB>(&leds[0][0], num_leds[0]);
  FastLED.addLeds<WS2812, LED_PIN + 1, GRB>(&leds[1][0], num_leds[1]);
  FastLED.addLeds<WS2812, LED_PIN + 2, GRB>(&leds[2][0], num_leds[2]);

  CRGB off = CRGB(0,0,0);
  for (int i = 0; i < NUM_STRIPS; i++) {
    setLED(&leds[i][0], &off, i);
  }

  delay(3000);
  Serial.begin(9600);
  Serial.println("begin");
}

void led_fire(int strip_id, struct led_trigger* trig) {
      CRGB out = CRGB(0, 0, 0);
      CRGB MAX = CRGB(255, 50, 0);
      CRGB MIN = CRGB(180, 10, 0);
      CRGB* pLed = &leds[strip_id][0];

      uint32_t color_range = random(0, 1000);
      if (color_range >= 900) {
        // 10% chance for blue
        MAX = CRGB(0, 100, 255);
        MIN = CRGB(0, 0, 150);
      } else if (color_range >= 700) {
        // another 20% chance for yellow
        MAX = CRGB(255, 255, 0);
        MIN = CRGB(255, 255, 0);
      } else {
        // remaining 70% change for redish...
        MAX = CRGB(255, 50, 0);
        MIN = CRGB(180, 10, 0);
      }

      random_color(&out, &MAX, &MIN);

      // dim output according to PWM value
      out = CRGB((out.red * PWM)/PWM_P, (out.green * PWM) / PWM_P, (out.blue * PWM) / PWM_P);

      setLED(pLed, &out, strip_id);
      trig->timestamp = millis() + random(10,100);
}

void fire_fixed_colors(int strip_id, struct led_trigger* trig) {
  CRGB out;
  uint32_t wait_ms = 0;
  CRGB* pLed = &leds[strip_id][0];

  switch(trig->state) {
    case 0:
      out = CRGB(255,200,200);
      wait_ms = random(0, 100);
      break;
    case 1:
      out = CRGB(255,150,0);
      wait_ms = random(20, 80);
      break;
    case 2:
      out = CRGB(255,0,0);
      wait_ms = random(20, 70);
      break;
    case 3:
      out = CRGB(0,0,255);
      wait_ms = random(20, 70);
      break;
  }

  trig->state = trig->state + 1;
  if (trig->state > 3) {
    trig->state = 0;
  }
  trig->timestamp = millis() + wait_ms;

  // dim output according to PWM value
  out = CRGB((out.red * PWM)/PWM_P, (out.green * PWM) / PWM_P, (out.blue * PWM) / PWM_P);

  setLED(pLed, &out, strip_id);
}

void position_light(int strip_id, struct led_trigger* trig, CRGB* color) {
  CRGB* on = color;
  CRGB off = CRGB(0,0,0);
  CRGB white = CRGB(255,255,255);
  CRGB* pLed = &leds[strip_id][0];

  if (GEAR_PWM > 1600) {
    setLED(pLed, &white, strip_id);
    return;
  }

  switch(trig->state) {
    case 0:
      setLED(pLed, &off, strip_id);
      trig->timestamp = millis() + 500;
      trig->state = 1;
      break;
    case 1:
      setLED(pLed, on, strip_id);
      trig->timestamp = millis() + 100;
      trig->state = 2;
      break;
    case 2:
      setLED(pLed, &off, strip_id);
      trig->timestamp = millis() + 100;
      trig->state = 3;
      break;
    case 3:
      setLED(pLed, on, strip_id);
      trig->timestamp = millis() + 100;
      trig->state = 0;
      break;
  }
}

void handle_strip(int strip_id) {
  led_trigger* trig = &trigger[strip_id];
  CRGB on;

  switch(strip_id) {
    case 0:
      fire_fixed_colors(strip_id, trig);
      break;
    case 1:
      on = CRGB::Green;
      position_light(strip_id, trig, &on);
      break;
    case 2:
      on = CRGB::Red;
      position_light(strip_id, trig, &on);
      break;
  }
}

void loop() {
  PWM = readPWM(PWM_PIN);

  if (PWM < PWM_MIN) {
    PWM = PWM_MIN;
  } else if (PWM > PWM_MAX) {
    PWM = PWM_MAX;
  }
  PWM -= PWM_MIN;

  GEAR_PWM = readPWM(GEAR_PWM_PIN);

  uint32_t now = millis();
  for (int i = 0; i < NUM_STRIPS; i++) {
    if (now < trigger[i].timestamp) {
      continue;
    }

    handle_strip(i);
  }

  led_show();

  /*static uint32_t round_trip = millis();
  uint32_t cycle_time = millis() - round_trip;
  Serial.println(cycle_time);
  round_trip = millis();*/
  delay(1);
}
