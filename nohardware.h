void nohardware_led_show(){
  Serial.print("RGB: ");
  for (int i = 0; i<num_leds[i]; i++) {
    Serial.print((uint8_t)leds[0][i].red); Serial.print(",");
    Serial.print((uint8_t)leds[0][i].green); Serial.print(",");
    Serial.print((uint8_t)leds[0][i].blue); Serial.print(",");
    Serial.print("  ");
  }
  Serial.println("");
}

uint32_t pulseInSim() {
  static uint32_t sim_PWM = 0;
  if (sim_PWM < 10000)
    sim_PWM = 20000;
  else
    sim_PWM = sim_PWM-1;

  //Serial.print("PWM:  "); Serial.println(sim_PWM);

  return sim_PWM/10;
}
